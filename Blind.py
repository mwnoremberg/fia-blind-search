import sys;
import math;
import random;
import time;
from heapq import heappush, heappop;

def swap(array, i, j):
    aux = array[i];
    array[i]=array[j];
    array[j]=aux;
    return array;

def geraEstadosPossiveis(estado):
    pos = estado.index(0);
    LARGURA = int(math.sqrt(len(estado)))
    ALTURA = LARGURA
    estadosPossiveis = [];

    if(pos==0):
        estadosPossiveis.append(swap(estado[:],pos, pos+1))
        estadosPossiveis.append(swap(estado[:],pos, pos+LARGURA))
    elif((pos+1)==(LARGURA*ALTURA)):
        estadosPossiveis.append(swap(estado[:],pos, pos-LARGURA));
        estadosPossiveis.append(swap(estado[:],pos, pos-1));
    elif((pos+1)%LARGURA == 0):
        estadosPossiveis.append(swap(estado[:],pos, pos+LARGURA));
        estadosPossiveis.append(swap(estado[:],pos, pos-1));
        if(pos+1>LARGURA):
            estadosPossiveis.append(swap(estado[:],pos, pos-LARGURA));
    elif((pos+1)%LARGURA == 1):
        estadosPossiveis.append(swap(estado[:],pos, pos+1));
        estadosPossiveis.append(swap(estado[:],pos, pos-LARGURA));
        if(pos+1<((LARGURA*ALTURA) - LARGURA)):
            estadosPossiveis.append(swap(estado[:],pos, pos+LARGURA));
    elif((pos+1)<LARGURA):
        estadosPossiveis.append(swap(estado[:],pos, pos+1));
        estadosPossiveis.append(swap(estado[:],pos, pos+LARGURA));
        estadosPossiveis.append(swap(estado[:],pos, pos-1));
    elif((pos+1)>((LARGURA*ALTURA) - LARGURA)):
        estadosPossiveis.append(swap(estado[:],pos, pos-LARGURA));
        estadosPossiveis.append(swap(estado[:],pos, pos-1));
        estadosPossiveis.append(swap(estado[:],pos, pos+1));
    else:
        estadosPossiveis.append(swap(estado[:],pos, pos+LARGURA));
        estadosPossiveis.append(swap(estado[:],pos, pos+1));
        estadosPossiveis.append(swap(estado[:],pos, pos-LARGURA));
        estadosPossiveis.append(swap(estado[:],pos, pos-1));
    return estadosPossiveis;


def embaralha(estado):
    estadosPossiveis = geraEstadosPossiveis(estado)
    estado = estadosPossiveis[random.randint(1, 100)%len(estadosPossiveis)][:]
    return estado;

def geraTabuleiro(array, tamanho):
    for i in range(1,tamanho*tamanho):
        array.append(i);
    array.append(0);
    return array;

def buscaEmLargura(raiz, nodosPercorrer, estadoFinal):
    nodosPercorrer.append(raiz)
    estadosVisitados = 0
    estadosPercorridos = []
    altura=-1
    while(len(nodosPercorrer)>0):
        nodo = nodosPercorrer.pop(0);
        estadosVisitados+=1
        if(int(sys.argv[3])==1):
            estadosPercorridos.append(nodo[0])
        if(nodo[0]==estadoFinal):
            return (nodo,estadosVisitados)
        estadosPossiveis = geraEstadosPossiveis(nodo[0]);
        for i in range(0,len(estadosPossiveis)):
            estado = estadosPossiveis[i][:]
            if(int(sys.argv[3])==1):
                if (estado not in estadosPercorridos):
                    nodosPercorrer.append((estado, nodo[1]+1))
            else:
                nodosPercorrer.append((estado, nodo[1]+1))
    return "nao encontrado"

def buscaEmProfundidade(raiz, nodosPercorrer, estadoFinal):
    nodosPercorrer.append(raiz)
    estadosPercorridos = []
    estadosPossiveis = []
    estadosVisitados = 0
    while(len(nodosPercorrer)>0):
        nodo = nodosPercorrer.pop()
        if(int(sys.argv[3])==1):
            estadosPercorridos.append(nodo[0])
        estadosVisitados+=1
        if(nodo[0]==estadoFinal):
            return (nodo,estadosVisitados)
        estadosPossiveis = geraEstadosPossiveis(nodo[0]);
        if(int(sys.argv[3])==1):
            for i in range(0,len(estadosPossiveis)):
               estado = estadosPossiveis[i][:]
               if (estado not in estadosPercorridos):
                   nodosPercorrer.append((estado, nodo[1]+1))
        else:
            while(len(estadosPossiveis)>0):
              estado = estadosPossiveis.pop(random.randint(1, 100)%len(estadosPossiveis))[:]
              nodosPercorrer.append((estado, nodo[1]+1))
    return "nao encontrado"

def buscaEmProfundidadeLimitada(raiz, nodosPercorrer, estadoFinal,alturaMaxima):
    nodosPercorrer.append(raiz)
    estadosPercorridos = []
    estadosPossiveis = []
    estadosVisitados = 0
    while(len(nodosPercorrer)>0):
        nodo = nodosPercorrer.pop()
        estadosVisitados+=1
        if(nodo[0]==estadoFinal):
            return (nodo,estadosVisitados)
        if(nodo[1]<alturaMaxima):
            estadosPossiveis = geraEstadosPossiveis(nodo[0]);
            for i in range(0,len(estadosPossiveis)):
                estado = estadosPossiveis[i][:]
                if(int(sys.argv[3])==1):
                    if (estado not in estadosPercorridos):
                        nodosPercorrer.append((estado, nodo[1]+1))
                else:
                    nodosPercorrer.append((estado, nodo[1]+1))
    return "nao encontrado"

def buscaEmProfundidadeIterativa(raiz, nodosPercorrer, estadoFinal):
    estadosPercorridos = []
    estadosPossiveis = []
    estadosVisitados = 0
    alturaMaxima = 0
    while 1:
        #print(alturaMaxima)
        nodosPercorrer.append(raiz)
        estadosPercorridos = []
        #print estadosPercorridos
        while(len(nodosPercorrer)>0):
            #print "nodos percorrer ",nodosPercorrer
            nodo = nodosPercorrer.pop()
            if(int(sys.argv[3])==1):
                estadosPercorridos.append(nodo[0])
            estadosVisitados+=1
            #print "nodo ",nodo
            if(nodo[0]==estadoFinal):
                return (nodo,estadosVisitados)
            if(nodo[1]<alturaMaxima):
                estadosPossiveis = geraEstadosPossiveis(nodo[0]);
                #print "estadospossivei",estadosPossiveis
                for i in range(0,len(estadosPossiveis)):
                    estado = estadosPossiveis[i][:]
                    if(int(sys.argv[3])==1):
                        if (estado not in estadosPercorridos):
                            nodosPercorrer.append((estado, nodo[1]+1))
                    else:
                        nodosPercorrer.append((estado, nodo[1]+1))

        alturaMaxima+=1
        #print estadosPercorridos
    return "nao encontrado"

#nao admissivel
def custoInversao(estado):
    resultado = 0
    for i in range(0, len(estado)):
        for j in range(i+1, len(estado)):
            valor = len(estado) if (estado[i]==0) else estado[i]
            valor2 = len(estado) if (estado[j]==0) else estado[j]
            if valor>valor2:
                resultado+=1
    return resultado

estadoFinal = geraTabuleiro([], int(sys.argv[1]))

def custoForaDePosicao(estado, estadoFinal=estadoFinal):
    resultado = 0
    for i in range(0, len(estado)):
        if estado[i]!=estadoFinal[i]:
            resultado+=1
    return resultado

def custoDistanciaPosicao(estado, estadoFinal=estadoFinal):
    resultado = 0
    for i in range(0, len(estado)):
        if estado[i]!=estadoFinal[i]:
            pos = estadoFinal.index(estado[i])
            linhaEstadoFinal = int(pos/int(sys.argv[1]))
            colunaEstadoFinal = int(pos%int(sys.argv[1]))
            linhaEstado = int(i/int(sys.argv[1]))
            colunaEstado = int(i%int(sys.argv[1]))
            distancia = abs(linhaEstadoFinal-linhaEstado)+abs(colunaEstadoFinal-colunaEstado)
            resultado+=distancia
    return resultado

def buscaAEstrela(raiz,nodosPercorrer,estadoFinal,funcaoCusto):
    estadosPercorridos = []
    estadosPossiveis = []
    estadosVisitados = 0
    heappush(nodosPercorrer, (funcaoCusto(raiz[0])+raiz[1],raiz))
    while nodosPercorrer:
        nodo = heappop(nodosPercorrer)[1]
        estadosVisitados+=1
        if(int(sys.argv[3])==1):
            estadosPercorridos.append(nodo[0])
        if(nodo[0]==estadoFinal):
            return (nodo,estadosVisitados)
        estadosPossiveis = geraEstadosPossiveis(nodo[0]);
        for i in range(0,len(estadosPossiveis)):
            estado = estadosPossiveis[i][:]
            if(int(sys.argv[3])==1):
                if (estado not in estadosPercorridos):
                    heappush(nodosPercorrer, (funcaoCusto(estado)+nodo[1]+1,(estado, nodo[1]+1)))
            else:
                heappush(nodosPercorrer, (funcaoCusto(estado)+nodo[1]+1,(estado, nodo[1]+1)))

def bestFirst(raiz,nodosPercorrer,estadoFinal,funcaoCusto):
    estadosPercorridos = []
    estadosPossiveis = []
    estadosVisitados = 0
    heappush(nodosPercorrer, (funcaoCusto(raiz[0]),raiz))
    while nodosPercorrer:
        nodo = heappop(nodosPercorrer)[1]
        estadosVisitados+=1
        if(int(sys.argv[3])==1):
            estadosPercorridos.append(nodo[0])
        if(nodo[0]==estadoFinal):
            return (nodo,estadosVisitados)
        estadosPossiveis = geraEstadosPossiveis(nodo[0]);
        for i in range(0,len(estadosPossiveis)):
            estado = estadosPossiveis[i][:]
            if(int(sys.argv[3])==1):
                if (estado not in estadosPercorridos):
                    heappush(nodosPercorrer, (funcaoCusto(estado),(estado, nodo[1]+1)))
            else:
                heappush(nodosPercorrer, (funcaoCusto(estado),(estado, nodo[1]+1)))

estadoInicial = estadoFinal[:]
for i in range(0, int(sys.argv[2])):
    estadoInicial = embaralha(estadoInicial);
print "Estado inicial:"
#estadoInicial=[0, 2, 3, 1, 5, 6, 4, 7, 8]
#estadoInicial = [8, 6, 7, 2, 5, 4, 3, 0, 1]
print estadoInicial
raiz = (estadoInicial, 0)

nodosPercorrerAEstrela = []
print "========================================="
print "Busca A Estrela (inversao):"
start = time.time()
resultado = buscaAEstrela(raiz,nodosPercorrerAEstrela,estadoFinal,custoInversao)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerAEstrela)
print "Tempo consumido: ", (end - start)
nodosPercorrerAEstrela = []
print "========================================="
print "Busca A Estrela (fora de posicao):"
start = time.time()
resultado = buscaAEstrela(raiz,nodosPercorrerAEstrela,estadoFinal,custoForaDePosicao)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerAEstrela)
print "Tempo consumido: ", (end - start)
nodosPercorrerAEstrela = []
print "========================================="
print "Busca A Estrela (distancia):"
start = time.time()
resultado = buscaAEstrela(raiz,nodosPercorrerAEstrela,estadoFinal,custoDistanciaPosicao)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerAEstrela)
print "Tempo consumido: ", (end - start)
nodosPercorrerProfundidadeIterativa = []
print "========================================="
print "Busca em Profundidade Iterativa:"
start = time.time()
resultado = buscaEmProfundidadeIterativa(raiz,nodosPercorrerProfundidadeIterativa,estadoFinal)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerProfundidadeIterativa)
print "Tempo consumido: ", (end - start)
nodosPercorrerLargura = []
print "========================================="
print "Busca em Largura:"
start = time.time()
resultado = buscaEmLargura(raiz,nodosPercorrerLargura,estadoFinal)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Fila: ", len(nodosPercorrerLargura)
print "Tempo consumido: ", (end - start)
nodosPercorrerProfundidadeLimitada = []
nodosPercorrerProfundidade = []
print "========================================="
print "Busca em Profundidade:"
start = time.time()
resultado = buscaEmProfundidade(raiz,nodosPercorrerProfundidade,estadoFinal)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerProfundidade)
print "Tempo consumido: ", (end - start)

nodosPercorrerBestFirst = []
print "========================================="
print "Best First (inversao):"
start = time.time()
resultado = bestFirst(raiz,nodosPercorrerBestFirst,estadoFinal,custoInversao)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerBestFirst)
print "Tempo consumido: ", (end - start)
nodosPercorrerBestFirst = []
print "========================================="
print "Best First (fora de posicao):"
start = time.time()
resultado = bestFirst(raiz,nodosPercorrerBestFirst,estadoFinal,custoForaDePosicao)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerBestFirst)
print "Tempo consumido: ", (end - start)
nodosPercorrerBestFirst = []
print "========================================="
print "Best First (distancia):"
start = time.time()
resultado = bestFirst(raiz,nodosPercorrerBestFirst,estadoFinal,custoDistanciaPosicao)
end = time.time()
print "Estados Visitados: ",resultado[1]
print "Altura: ",resultado[0][1]
print "Estados na Pilha: ", len(nodosPercorrerBestFirst)
print "Tempo consumido: ", (end - start)

# print "========================================="
# print "Busca em Profundidade Limitada:"
# start = time.time()
# resultado = buscaEmProfundidadeLimitada(raiz,nodosPercorrerProfundidadeLimitada,estadoFinal,10)
# end = time.time()
# if resultado=="nao encontrado":
#     print resultado
# else:
#     print "Estados Visitados: ",resultado[1]
#     print "Altura: ",resultado[0][1]
#     print "Estados na Pilha: ", len(nodosPercorrerProfundidadeLimitada)
# print "Tempo consumido: ", (end - start)
