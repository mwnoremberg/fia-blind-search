**Implementação do Trabalho do Quebra-cabeça deslizante para a disciplina de Fundamentos de Inteligência Artificial**

Para executar os algoritmos é necessário executar o seguinte comando:

```
> python Blind.py TAMANHO EMBARALHA REPETICAO
```

Onde TAMANHO é um inteiro que representa o tamanho do tabuleiro, EMBARALHA é um inteiro para a quantidade de movimentos aleatórios feitos para embaralhar o tabuleiro e REPETICAO define se os estados já visitados serão removidos (*1 para não visitar estados já visitados e qualquer outro número para revisitar os estados.*).

**Exemplos:**
```
> python Blind.py 3 43 0
```

O comando acima executa os algoritmos para o tamanho de tabuleiro 3x3, fazendo 43 movimentos aleatórios para embaralhar o tabuleiro, revisitando estados já visitados.

```
> python Blind.py 5 13 1
```

O comando acima executa os algoritmos para o tamanho de tabuleiro 5x5, fazendo 13 movimentos aleatórios para embaralhar o tabuleiro, não revisitando estados já visitados.
